package org.alittlebrighter.rpi.garagedoorremote;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;


public class SettingsActivity extends Activity {
    public final static String URL = "org.alittlebrighter.rpi.garagedoorremote.URL";
    private SharedPreferences prefs;
    EditText urlText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);

        urlText = (EditText) findViewById(R.id.settings_url);
        urlText.setText(prefs.getString(URL, this.getResources().getString(R.string.default_url)));
        urlText.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    saveURL();
                    return true;
                }
                return false;
            }
        });

        findViewById(R.id.button1).setOnClickListener(getCustomClickListener(0, false));
        findViewById(R.id.button1Force).setOnClickListener(getCustomClickListener(0, true));
        findViewById(R.id.button2).setOnClickListener(getCustomClickListener(1, false));
        findViewById(R.id.button2Force).setOnClickListener(getCustomClickListener(1, true));
    }

    @Override
    protected void onPause() {
        super.onPause();

        saveURL();
    }

    private void saveURL() {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(URL, urlText.getText().toString());
        editor.commit();
    }

    private View.OnClickListener getCustomClickListener(final int door, final boolean force) {
        final Context ctx = this;
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(TriggerReceiver.ACTION);
                intent.putExtra(TriggerReceiver.URL, urlText.getText().toString());
                intent.putExtra(TriggerReceiver.DOOR, door);
                intent.putExtra(TriggerReceiver.FORCE, force);

                sendBroadcast(intent);
            }
        };
    }
}
