package org.alittlebrighter.rpi.garagedoorremote;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;


public class RemoteButtonsWidget extends AppWidgetProvider {
    static String TAG = "RemoteButtonsWidget";

    private static final String Button1Click = "button1Click";
    private static final String Button2Click = "button2Click";
    private static final String Button1Force = "button1Force";
    private static final String Button2Force = "button2Force";

    protected static PendingIntent getPendingIntent(Context context, String action) {
        Log.i(TAG, "Action: " + action);
        Intent intent = new Intent();
        intent.setAction(TriggerReceiver.ACTION);
        int requestCode = 0;

        switch (action) {
            case Button1Click:
                Log.d(TAG, "button 1 clicked");
                intent.putExtra(TriggerReceiver.DOOR, 0);
                requestCode = 0;
                break;
            case Button2Click:
                Log.d(TAG, "button 2 clicked");
                intent.putExtra(TriggerReceiver.DOOR, 1);
                requestCode = 1;
                break;
            case Button1Force:
                Log.d(TAG, "button 1 force clicked");
                intent.putExtra(TriggerReceiver.DOOR, 0);
                intent.putExtra(TriggerReceiver.FORCE, true);
                requestCode = 2;
                break;
            case Button2Force:
                Log.d(TAG, "button 2 force clicked");
                intent.putExtra(TriggerReceiver.DOOR, 1);
                intent.putExtra(TriggerReceiver.FORCE, true);
                requestCode = 3;
                break;
        }

        return PendingIntent.getBroadcast(context, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        Log.d("test", "onUpdate");

        // There may be multiple widgets active, so update all of them
        final int N = appWidgetIds.length;
        for (int i = 0; i < N; i++) {
            updateAppWidget(context, appWidgetManager, appWidgetIds[i]);
        }
    }

    protected void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.remote_buttons_widget);

        views.setOnClickPendingIntent(R.id.button1, getPendingIntent(context, Button1Click));
        views.setOnClickPendingIntent(R.id.button2, getPendingIntent(context, Button2Click));
        views.setOnClickPendingIntent(R.id.button1Force, getPendingIntent(context, Button1Force));
        views.setOnClickPendingIntent(R.id.button2Force, getPendingIntent(context, Button2Force));

        Log.d(TAG, "setup nearly complete");

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }
}


