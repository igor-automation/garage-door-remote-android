package org.alittlebrighter.rpi.garagedoorremote;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by adam on 10/22/15.
 */
public class TriggerReceiver extends BroadcastReceiver {
    public final static String TAG = "TriggerReceiver";

    public final static String ACTION = "org.alittlebrighter.rpi.GARAGE_DOOR_REMOTE";
    public final static String URL = "org.alittlebrighter.rpi.garagedoorremote.URL";
    public final static String DOOR = "org.alittlebrighter.rpi.garagedoorremote.DOOR";
    public final static String FORCE = "org.alittlebrighter.rpi.garagedoorremote.FORCE";

    private Context ctx;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive");
        this.ctx = context;

        String url = "";
        String forcedURL = intent.getStringExtra(URL);
        if (forcedURL != null && !forcedURL.equals("")) {
            url = forcedURL;
        } else {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this.ctx);
            url = prefs.getString(SettingsActivity.URL, this.ctx.getResources().getString(R.string.default_url));
        }
        int door = intent.getIntExtra(DOOR, -1);
        boolean force = intent.getBooleanExtra(FORCE, false);
        this.sendHTTPCommand(url, door, force);
        Log.d(TAG, String.format("URL: %s, Door: %d, Force: %b", url, door, force));
    }

    private void sendHTTPCommand(String url, final int doorNumber, final boolean force) {
        RequestQueue queue = Volley.newRequestQueue(ctx);
        StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Success: " + response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(ctx, ctx.getString(R.string.toast_error), Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("door", Integer.toString(doorNumber));
                params.put("force", String.valueOf(force));

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);
    }
}
